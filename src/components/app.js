import React, { Component } from 'react';
import CommentBox from './CommentBox';
import CommetnList from './CommentList';

export default class App extends Component {
  render() {
    return (
      <div>
        <CommentBox />
        <CommetnList />
      </div>
    );
  }
}
