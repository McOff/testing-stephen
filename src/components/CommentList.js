import React from 'react';
import { connect } from 'react-redux';

const CommentList = props => {
  const comments = props.comments.map((comment, idx) =>
    <li key={idx}>
      {comment}
    </li>
  );
  return (
    <ul className="comment-list">
      {comments}
    </ul>
  );
};

const mapStateToProps = state => ({
  comments: state.comments
});

export default connect(mapStateToProps)(CommentList);
