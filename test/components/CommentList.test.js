import { renderComponent, expect } from '../test_helper';
import CommentList from '../../src/components/CommentList';

describe('CommentList', () => {
  let component;

  beforeEach(() => {
    let props = { comments: ['New comment', 'Another new comment'] };
    component = renderComponent(CommentList, null, props);
  });

  it('shows <li> for each component', () => {
    expect(component.find('li').length).to.equal(2);
  });

  it('shows eact comment that is provided', () => {
    expect(component.find('li')).to.contain('New comment');
    expect(component.find('li')).to.contain('Another new comment');
  });
});
